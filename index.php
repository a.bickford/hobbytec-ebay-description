<table align="center" style="width: 600px; font-family: Tahoma; border-spacing: 0px;">
<tbody><tr>
<td style="padding: 10px; border-top-color: rgb(204, 204, 204); border-right-color: rgb(204, 204, 204); border-left-color: rgb(204, 204, 204); border-top-width: 1px; border-right-width: 1px; border-left-width: 1px; border-top-style: solid; border-right-style: solid; border-left-style: solid;">
<img style="float: left;" src="http://cdn.shopify.com/s/files/1/0427/6925/files/hobbytec.png?2268">
</td>
</tr>
<tr>
<td style="background: rgb(251, 251, 251); padding: 20px; border: 1px solid rgb(204, 204, 204); border-image: none;">
<h1 style="text-align: center; color: #EB68A1;">
[[ PRODUCT NAME HERE ]]
</h1>
<center>
<img src="[[ PRODUCT IMAGE HERE ]]">
</center>
<br>
<h3 style="color: #EB68A1; font-family: Tahoma;">
Information
</h3>
<ul style="color:#aa0079;">
<li>[[ SELLING POINT 1 ]]</li>
</ul>
<ul style="color: #aa0079;">
<li>[[ SELLING POINT 2 ]]</li>
</ul>
<ul style="color: #aa0079;">
<li>[[ SELLING POINT 3 ]]</li>
</ul>
<center><h3 style="text-align: center; color: #aa0079;"><font size="4">[[ RRP PRICE ]]</font></h3></center>
</td></tr>
<tr>
<td style="height: 20px;">
</td>
</tr>
<tr>
<td style="background: rgb(251, 251, 251); padding: 10px; border: 1px solid rgb(204, 204, 204); border-image: none;">
<h2 style="color: #EB68A1;">Deliveries</h2>
<p>
</p><h4 style="color: #CC0079;">DELIVERY WITHIN MAINLAND UK</h4>
<p style="color:#1B1B1B">
All orders are dispatched using 2nd class Royal Mail. Please be patient and allow up to 3 working days.
<br><br>
Please ensure that there is someone at the delivery address to sign for the packages between 8am and 5pm.
</p>
<h4 style="color: #CC0079;">COLLECTION</h4>
<p style="color:#1B1B1B;">
If preferred, orders may be collected from the trade counter from 08.00 -16.30 Mon-Fri by prior arrangement.
</p>
</td>
</tr>
<tr>
<td style="height: 20px;">
</td>
</tr>
</tbody>
</table>